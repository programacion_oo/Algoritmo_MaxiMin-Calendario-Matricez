/*Nombre: Andres Felipe Wilches Torres
 * fecha: 3/04/2018
 * Nombre programa: Calculo mes
 */
package Ejercicio4;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class Main {
	public static void main (String arg[]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int pregunta;
		do {
			int m1,n1,m2,n2;
			System.out.println("Ingrese el tama�o de la matriz 1 (tama�o m*n):  ");
			System.out.print("matriz 1 tama�o de 'm': ");
			m1 = Integer.parseInt(in.readLine());
			while (m1 <= 0) {
				System.out.print("numero ingresado incorrecto, ingrese nuevamente el tama�o 'm' de la matriz 1: ");
				m1 = Integer.parseInt(in.readLine());
			}
			System.out.print("matriz 1 tama�o de 'n': ");
			n1 = Integer.parseInt(in.readLine());
			while (n1 <= 0) {
				System.out.print("numero ingresado incorrecto, ingrese nuevamente el tama�o 'n' de la matriz 1: ");
				n1 = Integer.parseInt(in.readLine());
			}
			System.out.println("Ingrese el tama�o de la matriz 2 (tama�o m*n):  ");
			System.out.print("matriz 2 tama�o de 'm': ");
			m2 = Integer.parseInt(in.readLine());
			while (m2 <= 0) {
				System.out.print("numero ingresado incorrecto, ingrese nuevamente el tama�o 'm' de la matriz 2: ");
				m2 = Integer.parseInt(in.readLine());
			}
			System.out.print("matriz 2 tama�o de 'n': ");
			n2 = Integer.parseInt(in.readLine());
			while (n2 <= 0) {
				System.out.print("numero ingresado incorrecto, ingrese nuevamente el tama�o 'n' de la matriz 2: ");
				n2 = Integer.parseInt(in.readLine());
			}
			if (n1 != m2) {
				System.out.println("El tama�o de las matricez no coincide para hacer una multiplicacion.");
			}else {
				double [][] matriz1 = new double [m1][n1];
				double [][] matriz2 = new double [m2][n2];
				for (int i = 0; i<m1; i++){
					for (int j = 0; j<n1; j++){
						System.out.print("Digite el contenido de la matriz 1 posicion "+j+1+" "+i+1+" ");
						matriz1 [i][j] = Double.parseDouble(in.readLine());
					}
					System.out.print("\n");
				}
				for (int a = 0; a<m2; a++){
					for (int b = 0; b<n2; b++){
						System.out.print("Digite el contenido de la matriz 2 posicion "+b+1+" "+a+1+" ");
						matriz2 [a][b] = Double.parseDouble(in.readLine());
					}
					System.out.print("\n");
				}
				OperacionMatricez objeto = new OperacionMatricez(matriz1,matriz2);
				objeto.setM1(m1);
				objeto.setN1(n1);
				objeto.setM2(m2);
				objeto.setN2(n2);
				if (n1 == n2 && m1 == m2 && m1 == n1 && m2 == n2) {
					objeto.mcuadradas(matriz1,matriz2);
				}else {
					objeto.multiplicacionmatricez(matriz1, matriz2);
				}
			}
			System.out.println("�Desea ejecutar el programa de nuevo?(si=1)");
			pregunta = Integer.parseInt(in.readLine());
		}while (pregunta==1);
	}
}
