package Ejercicio1;

public class Calculo {
	
	private int max;
	private int min;
	private int[]num;
	private String posmax="";
	private String posmin="";
	
	public int maximo() {
		int mayor=0;
		for(int i=0;i< num.length;i++) {
			if(num[i]>num[mayor])
				mayor=i;
		}
		max=num[mayor];
		return max;
	}
	public int minimo() {
		int menor=0;
		for(int i=0;i< num.length;i++) {
			if(num[i]<num[menor]) {
				menor=i;
			}
		}
		min=num[menor];
		return min;
	}
	
	public Calculo(){
		this.max=max;
		this.min=min;
	}
	public String Posicionmax() {
		int rep=0;
		for(int i=0;i<num.length;i++) {
			if(num[i]==max) {
				rep++;
				posmax=posmax+" "+i;
			}
		}
		return ("El numero mayor es "+max+". Se repite "+rep+" veces, y se encuentra en las posiciones | "+posmax+" |");
	}
	public String Posicionmin() {
		int rep=0;
		for(int i=0;i<num.length;i++) {
			if(num[i]==min) {
				rep++;
				posmin=posmin+" "+i;
			}
		}
		return ("El numero menor es: "+min+". Se repite "+rep+" veces, y se encuentra en las posiciones | "+posmin+" |");
	}
	public int getMax() {
		return max;
	}
	public void setMax(int max) {
		this.max = max;
	}
	public int getMin() {
		return min;
	}
	public void setMin(int min) {
		this.min = min;
	}
	public int[] getNum() {
		return num;
	}
	public void setNum(int[] num) {
		this.num = num;
	}
	public String getPosmax() {
		return posmax;
	}
	public void setPosmax(String posmax) {
		this.posmax = posmax;
	}
	public String getPosmin() {
		return posmin;
	}
	public void setPosmin(String posmin) {
		this.posmin = posmin;
	}
	
	
}
