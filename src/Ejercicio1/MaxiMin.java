/*Andres Felipe Wilches Torres
 * fecha 5/04/18
 * Maximos y minimos
 */
package Ejercicio1;

import java.util.*;

public class MaxiMin {
	public static void main(String[] arg) {
		Scanner ent = new Scanner(System.in);
		int pregunta;
		do {
			Calculo operacion= new Calculo();
			int []num;
			int tam;
			System.out.print("Digite la cantidad de numeros a ingresar: ");
			tam=ent.nextInt();
			num=new int [tam];
			for(int i=0;i<tam;i++) {
				int j=i+1;
				System.out.print("Digite el numero "+j+": ");
				num[i]=ent.nextInt();
			}
			operacion.setNum(num);
			operacion.maximo();
			System.out.println(operacion.Posicionmax());
			operacion.minimo();
			System.out.println(operacion.Posicionmin());
			// se pide si desea realizar otro calculo
			System.out.println("\n �Desea ejecutar el programa de nuevo?(si=1)");
			pregunta = ent.nextInt();
			// se verifica que sea la opcion correcta
		} while (pregunta==1);
		//se termina el ciclo
	}

}
