/*Nombre: Andres Felipe Wilches Torres
 * fecha: 3/04/2018
 * Nombre programa: Calculo mes
 */
package Ejercicio3;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class Main {
	public static void main (String arg[]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int pregunta;
		do{
			int opcion;
			System.out.print("Ingrese el numero del mes que quiere saber: ");
			opcion = Integer.parseInt(in.readLine());
			while (opcion <= 0 || opcion > 12) {
				System.out.print("Mes inexistente, ingrese una opcion valida (1-12):  ");
				opcion = Integer.parseInt(in.readLine());
			}
			RetornoMes Objeto = new RetornoMes(opcion);
			System.out.println("�Desea ejecutar el programa de nuevo?(si=1)");
			pregunta = Integer.parseInt(in.readLine());
		}while(pregunta==1);
	}
}
