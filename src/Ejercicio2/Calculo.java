package Ejercicio2;

public class Calculo {
	
	private double definitiva;
	private String[][] datos;

	public void Calcular(int est) {
		double nota1=Double.parseDouble(datos[est][2]);
		double nota2=Double.parseDouble(datos[est][3]);
		double nota3=Double.parseDouble(datos[est][4]);
		definitiva=(nota1*0.3)+(nota2*0.3)+(nota3*0.4);
		definitiva=Math.round(definitiva*10)/10.0;
	}
	
	public Calculo() {
		
	}
	
	public double getDefinitiva() {
		return definitiva;
	}


	public void setDefinitiva(double definitiva) {
		this.definitiva = definitiva;
	}


	public String[][] getDatos() {
		return datos;
	}

	public void setDatos(String[][] datos) {
		this.datos = datos;
	}
	

}
