/*Andres Felipe Wilches Torres
 * 5/04/2018
 * estudiantes
 */
package Ejercicio2;
import java.util.*;
public class Datos {
	public static void main(String[] args) {
		Scanner ent = new Scanner(System.in);
		int pregunta;
		do {
			Calculo operacion = new Calculo();
			String datos[][] = new String[3][5];
			double promedio = 0;
			for (int i = 0; i < 3; i++) {
				int j = i + 1;
				double n1, n2, n3;
				System.out.println("Digite los datos del estudante " + j + ":");
				System.out.print("Nombre: ");
				datos[i][0] = ent.next();
				System.out.print("Apellido: ");
				datos[i][1] = ent.next();
				System.out.print("Nota 1: ");
				datos[i][2] = ent.next();
				n1 = Double.parseDouble(datos[i][2]);
				while (n1 < 0 | n1 > 5) {
					System.out.print("La nota debe ser mayor de 0 y menor que 5.\n Nota 1: ");
					datos[i][2] = ent.next();
					n1 = Double.parseDouble(datos[i][2]);
				}
				System.out.print("Nota 2: ");
				datos[i][3] = ent.next();
				n2 = Double.parseDouble(datos[i][2]);
				while (n2 < 0 | n2 > 5) {
					System.out.print("La nota debe ser mayor de 0 y menor que 5.\n Nota 2: ");
					datos[i][2] = ent.next();
					n2 = Double.parseDouble(datos[i][2]);
				}
				System.out.print("Examen Final: ");
				datos[i][4] = ent.next();
				n3 = Double.parseDouble(datos[i][2]);
				while (n3 < 0 | n3 > 5) {
					System.out.print("La nota debe ser mayor de 0 y menor que 5.\n Nota 2: ");
					datos[i][2] = ent.next();
					n3 = Double.parseDouble(datos[i][2]);
				}
			}
			operacion.setDatos(datos);
			for (int i = 0; i < 3; i++) {
				operacion.Calcular(i);
				System.out.println(datos[i][0] + " " + datos[i][1] + " " + operacion.getDefinitiva());
				promedio = promedio + operacion.getDefinitiva();
			}
			promedio = Math.round((promedio / 3) * 10) / 10.0;
			System.out.println("El promedio de los estudiantes es de: " + promedio);

			// se pide si desea realizar otro calculo
			System.out.println("\n �Desea realizar otro calculo?(si=1)");
			pregunta = ent.nextInt();
			// se verifica que sea la opcion correcta
		} while (pregunta==1);
		// se termina el ciclo
	}

}
